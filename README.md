# Introduction

This project contains configuration files for monitoring a remote FreedomBox machine with Nagios. The project consists of two components: **Nagios4** (web) and **Nagios Remote Plugin Executor Server.**

# Nagios 4

This machine collects and displays status data of a remote FreedomBox installation and will notify via email should anything break down.

## Prerequisites

1. Setup FreedomBox and install Postfix/Dovecot.
2. Create a user named **nagiosadmin**
3. Install the following packages: `$ sudo apt-get install nagios4 monitoring-plugins nagios-nrpe-plugin`
4. Copy the files from this project's `web/` directory into their appropriate paths.
5. Modify the email address in `/etc/nagios4/objects/contacts.cfg`. This will be the address that receives alerts.
6. In the host definition of `/etc/nagios4/objects/freedombox.cfg` change the address to be the domain of the remote FreedomBox that will be monitored.
7. Edit `etc/nagios4/resource.cfg` and specify a password for $USER4$
8. Test if all configurations are correct: `nagios4 -v /etc/nagios4/nagios.cfg`
9. Restart Nagios: `$ sudo systemctl restart nagios4`
10. Make sure the apache configuration is enabled and reload the web server: `$ sudo a2enconf nagios4-cgi && sudo systemctl reload apache2`

# Nagios Remote Plugin Executor Server

1. On a separate machine from Nagios 4, setup FreedomBox and install all the available apps.
2. Create a user named **radicale-user** and give it the same password as in previous section's step 7.
2. Install the necessary nagios packages: `$ sudo apt-get install monitoring-plugins-systemd nagios-nrpe-server monitoring-plugins`.
3. Copy the files from this project's `nagios-remote-server/` directory into their appropriate paths.
4. Edit the database credentials in `etc/nagios/nrpe_local.cfg`
5. Restart the service `$ sudo systemctl restart nagios-nrpe-server.service`

<img src="screenshots/nagios4_main_interface.png" width="700px">
<img src="screenshots/nagios4_service_groups.png" width="700px">
<img src="screenshots/email.png" width="700px">
